package com.gs.ddf.testcases;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.gs.ddf.base.TestBase;

public class AddCustomerTest extends TestBase {

	@Test(dataProvider="getData")
	public void addCustomer(String firstName, String lastName,String postCode, String alertText) throws InterruptedException {
		
		click(By.xpath(OR.getProperty("addCustomerBtnXpath")));
		//driver.findElement(By.xpath(OR.getProperty("addCustomerBtnXpath"))).click();
		enterText(By.xpath(OR.getProperty("firstNameXpath")), firstName);
		enterText(By.xpath(OR.getProperty("lastNameXpath")), lastName);
		enterText(By.xpath(OR.getProperty("pstCodeXpath")), postCode);
		
		click(By.xpath(OR.getProperty("addCustBtnXpath")));
		
		Thread.sleep(4000);
		
		Alert alert = wait.until(ExpectedConditions.alertIsPresent());
		
		Assert.assertTrue(alert.getText().contains(alertText));
		alert.accept();
		Thread.sleep(3000);
	}
	
	@DataProvider
	public Object[][] getData(){
		String sheetName = "AddCustomerTest";
		int rows = excel.getRowCount(sheetName);
		int cols = excel.getColumnCount(sheetName);
		
		Object[][] data = new Object[rows-1][cols];
		
		for (int rowNum = 2; rowNum <=rows;rowNum++) {
			for(int colNum=0;colNum<cols;colNum++) {
				data[rowNum-2][colNum]=excel.getCellData(sheetName, colNum, rowNum);
				
			}
		}
		return data;
	}
	
}
